package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        if (inputNumbers.contains(null)) throw new CannotBuildPyramidException();
        int counter = 0;
        int height = 0; // высота пирамиды
        int width = 0; // ширина пирамиды
        int summa = 0; // количество переданных чисел
        for (int i = 1; i < (Integer.MAX_VALUE - 1) / 2; i++) { // чтобы построрить пирамиду, количество переданных числов должно соответсвовать ряду 1, 3, 6, 10, 15, 21
            summa += i;                                           // проверяем соответствует ли переданных список условию
            if (summa == inputNumbers.size()) {
                height = i;
                width = 2*i - 1;
                break;
            } else if (summa > inputNumbers.size()) {
                throw new CannotBuildPyramidException();
            }
        }
        int [] [] pyramidArray = new int[height][width];
        try {
            Collections.sort(inputNumbers);
        } catch (OutOfMemoryError e) {
            throw new CannotBuildPyramidException();
        }
        int firstElementPosition = width / 2;           // вычисление позиции верхнего элемента
        pyramidArray[0][firstElementPosition] = inputNumbers.get(0);
        counter++;

        for (int i = 1; i < height; i++)
            for ( int j = firstElementPosition - i; j <= i + firstElementPosition; j = j + 2) {
                pyramidArray[i][j] = inputNumbers.get(counter);
                counter++;
            }
        return pyramidArray;
    }

}


