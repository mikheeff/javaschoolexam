package com.tsystems.javaschool.tasks.zones;

import java.util.*;

public class RouteChecker {


    /**
     * Checks whether required zones are connected with each other.
     * By connected we mean that there is a path from any zone to any zone from the requested list.
     *
     * Each zone from initial state may contain a list of it's neighbours. The link is defined as unidirectional,
     * but can be used as bidirectional.
     * For instance, if zone A is connected with B either:
     *  - A has link to B
     *  - OR B has a link to A
     *  - OR both of them have a link to each other
     *
     * @param zoneState current list of all available zones
     * @param requestedZoneIds zone IDs from request
     * @return true of zones are connected, false otherwise
     */

    public boolean checkRoute(List<Zone> zoneState, List<Integer> requestedZoneIds){
        // TODO : Implement your solution here
       Graph graphObj = new Graph(zoneState); // Инициализация графа
        return graphObj.areVertexesConnected(requestedZoneIds); // проверка можно ли путешествовать через заданные зоны
    }
    class Graph {
        private int nVerts;   // Количество вершин графа
        private List<Zone> zoneList; // Список всех зон
        private int matrix[][]; // матрица смежности

        public Graph(List<Zone> zoneList) {
            nVerts = zoneList.size();
            this.zoneList = zoneList;
            matrix = new int[nVerts][nVerts];
            setMatrix();
        }
        public void setMatrix(){
            for (int i = 0; i < zoneList.size(); i++) {  // Создание связей между вершинами графа (зонами) и запись их
                for (int j = 0; j < zoneList.get(i).getNeighbours().size(); j++) {  // в матрице смежности
                    addEdge(zoneList.get(i).getId(),zoneList.get(i).getNeighbours().get(j));
                }
            }
        }
        public void addEdge(int start, int end) {   // функция записи связей между верщинами
            start--;
            end--;
            matrix[start][end] = 1;
            matrix[end][start] = 1;
        }
        public boolean areVertexesConnected (List<Integer> reqZonesList){  // функция, для проверки возможно ли путшествовать по запрошенным зонам
            if (reqZonesList.size()==1) {                  //в случае если передана одна зона, проверяем если ли такая на карте и если есть возвращаем true
                for (int i = 0; i < zoneList.size(); i++) {
                    if (reqZonesList.get(0) == zoneList.get(i).getId()){
                        return true;
                    }
                }
                return false;
            }      
            for (int j = 0; j < reqZonesList.size(); j++) {   // перебор строк матрицы смежности, для проверки соединены ли запрашиваемые зоны
                for (int k = 0; k < reqZonesList.size();k++) {
                    if (matrix[reqZonesList.get(j) - 1][reqZonesList.get(k)-1]==1) {
                        break;
                    }
                    if (k==reqZonesList.size()-1) // в случае если у одной зоны после перебора строки не обнаружено связей с другими
                        return false;               // возвращается false
                }
            }

            visitNeighbours(reqZonesList.get(0)-1); // далее происходит рекурсивный обход графа, начиная с первого элемента списка переданных зон
                                                    // все пройденные вершины помечаются как посещённые

            for (int i = 0; i < reqZonesList.size(); i++) { // Проверка на случай, если граф является несвязным. Здесь определяется
                if (zoneList.get(reqZonesList.get(i)-1).getWasVisited()==false) // входят ли переданные зоны в связную часть несвязного графа
                    return false;                                               // (есть ли путь между зонами)
            }

            for (Zone z : zoneList) // сброс флагов
                z.setWasVisited(false);

            return true;
        }
        public void visitNeighbours (int vertex) {  // рекурсивный медтод обхода графа
            zoneList.get(vertex).setWasVisited(true);
            for (int i = 0; i < nVerts; i++) {
                if (matrix[vertex][i]==1&&!zoneList.get(i).getWasVisited()){
                    visitNeighbours(i);
                }
            }
        }
    }

}
