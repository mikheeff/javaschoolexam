package com.tsystems.javaschool.tasks.zones;

import java.util.List;

public class Zone {

    private final Integer id;

    private final List<Integer> neighbours;

    private boolean wasVisited;

    public Zone(Integer id, List<Integer> neighbours) {
        this.id = id;
        this.neighbours = neighbours;
        this.wasVisited = false;
    }

    public int getId() {
        return id;
    }

    public List<Integer> getNeighbours() {
        return neighbours;
    }

    public void setWasVisited(boolean wasVisited) { this.wasVisited = wasVisited; }

    public boolean getWasVisited() { return wasVisited; }
}
